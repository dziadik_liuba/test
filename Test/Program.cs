﻿using System;
using System.Collections.Generic;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите минимальное число(целое и положительное):");
            var minStr = Console.ReadLine();
            int min;
            var minCorrect = int.TryParse(minStr, out min);
            while (!minCorrect)
            {
                Console.WriteLine("Введены некоректные данные. Число должно быть целое и положительное.");
                minStr = Console.ReadLine();
                minCorrect = int.TryParse(minStr, out min);
            }

            Console.WriteLine("Введите максимально число(целое и положительное):");
            var maxStr = Console.ReadLine();
            int max;
            var maxCorrect = int.TryParse(maxStr, out max);
            while (!maxCorrect)
            {
                Console.WriteLine("Введены некоректные данные. Число должно быть целое и положительное.");
                maxStr = Console.ReadLine();
                maxCorrect = int.TryParse(maxStr, out max);
            }

            Console.WriteLine("Введите необходимую сумму чисел(целое и положительное):");
            var sumStr = Console.ReadLine();
            int sum;
            var sumCorrect = int.TryParse(sumStr, out sum);
            while (!sumCorrect)
            {
                Console.WriteLine("Введены некоректные данные. Число должно быть целое и положительное.");
                sumStr = Console.ReadLine();
                sumCorrect = int.TryParse(sumStr, out sum);
            }

            var result = Logic(min, max, sum);

            Console.WriteLine("[{0}]", string.Join(", ", result));
        }

        static int[] Logic(int min, int max, int sum)
        {
            int length = max - min + 1;
            var arr = new int[length];
            var result = new List<int>();

            for (var i = 0; i < length; i++)
            {
                arr[i] = min + i;

                var num = arr[i];
                var sumNum = 0;

                while (num > 0)
                {
                    sumNum +=num % 10;
                    num = num / 10;
                }

                if (sumNum == sum)
                {
                    result.Add(arr[i]);
                }
            }

            return result.ToArray();
        }
    }
}
